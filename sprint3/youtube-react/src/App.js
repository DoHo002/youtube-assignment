import React, { Component } from 'react';
import './App.css';
import NavDisplay from './Nav.js';
import { BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import Page from './Page.js'
import Upload from './Upload.js';
class App extends Component {
  state = {
    videosArray: [],
    mainVideo: { comments:[] }
  }
  getMainVideo = (currentUrl) => {
    fetch("http://localhost:8080/videos/"+ currentUrl )
    .then(response => response.json())
    .then(data => {
      this.setState({
        mainVideo: data
      });
    });
  }
componentDidMount(){
    fetch("http://localhost:8080/videos")
    .then(response => response.json())
    .then(data => {
      this.setState({
        videosArray: data,
        
      });
    });
    
  }
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <NavDisplay />
            <Switch>
              <Route path={'/main/:vidid'} exact render={ (props) => <Page getMainVideo={ this.getMainVideo } match={props.match} mainVideo={this.state.mainVideo} videosArray={this.state.videosArray} commentsData={ this.state.mainVideo.comments } />}  />
              <Route path={'/upload'} exact component={Upload} />
              <Route path={'/'} exact render={() => <Redirect to='/main/0'/>} />
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;