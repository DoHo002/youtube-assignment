import React, { Component } from 'react'
export default class CommentCard extends Component {
  render() {
    return (
        <div className='comment-card'>
          <div className="comment-titles">
                <div className ='gibbonsCircle2'>
                  <img className = 'gibbons' src = '../Assets/Images/john_gibbons.jpg' />
                </div>
                <div className='comment-card__name'>
                { this.props.name }
                </div>
                <div className='comment-card__timestamp'>
                { this.props.timestamp }
                </div>      
          </div>      
              <div className='comment-card__comment'>
              { this.props.comment }
              </div>
        </div>
    )
  }
}