import React, { Component } from 'react'
import CommentCard from './CommentCard';
export default class Comments extends Component {
constructor(){
  super();
  this.commentInput = React.createRef();
}
submitHandler = (event) => {
  event.preventDefault();
  let postComment = this.commentInput.current.value;
  let newComment = { 
    name: 'BillyBob',
    timestamp: '1530744338879',
    comment: postComment
    }
  const newPost = {
    body: JSON.stringify(newComment),
    method: 'POST',
    headers:{'Content-Type': 'Application/JSON'}
    }
  fetch('http://localhost:8080/videos/' + this.props.match.params.vidid, newPost)
    .then(response => response.json())
    .then(data => {
    this.props.getMainVideo(this.props.match.params.vidid)
    })
    }
  render(){
    let commentInfo = this.props.commentData
    let commentBlocks = []
    for( let i = 0; i < commentInfo.length; i++){
    let coms = < CommentCard name={ commentInfo[i].name }
    comment={ commentInfo[i].comment }
    timestamp={ commentInfo[i].timestamp }
    />
    commentBlocks.push(coms);                  
    }
    return (
      <div className='comment-boxsection'>
        <div className = 'comment-textbox'>
          <div className ='gibbonsCircle2'>
              <img className = 'gibbons' src = '../Assets/Images/john_gibbons.jpg'/>
          </div>
         </div>
         <div>
            <textarea className='comments-words' type='text' placeholder = 'Add a public comment' ref={this.commentInput}/>
            <div className='enter-buttons'>
              <button className='cancel' > CANCEL</button>
              <button onClick={ this.submitHandler } type='button' className='comment'>
                COMMENT
              </button>
            </div>
         </div>
         <div className='comments-loop'>
          { commentBlocks }
        </div>
      </div>
    )
  }
}
