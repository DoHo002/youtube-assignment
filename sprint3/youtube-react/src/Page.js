import React, { Component } from 'react'
import Main from './Main.js';
import SideBar from './SideBar.js';
export default class Page extends Component {
  componentDidMount(){
    this.props.getMainVideo(this.props.match.params.vidid)
  }
  componentDidUpdate(prevProps){
    if(prevProps.match.params.vidid != this.props.match.params.vidid){
      this.props.getMainVideo(this.props.match.params.vidid)
    }
  }
render() {
    return (
        <div className='combine'> 
        <Main mainVideo={this.props.mainVideo} commentsData={ this.props.commentsData } match={ this.props.match } getMainVideo={ this.props.getMainVideo } />
        <SideBar videoArray={this.props.videosArray} />
      </div> 
    )
  }
}
