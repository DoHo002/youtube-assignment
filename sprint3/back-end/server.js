const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const videos = require('./videos');
const videoLinks = require('./videoLinks');
app.use(express.static('public'));
app.use(bodyParser());
app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
  res.header("Access-Control-Allow-Methods: GET, POST, HEAD, OPTIONS, PUT, DELETE");
  next();
});
app.get('/videos/:id', (req, res) =>{
  let videoId = req.params.id;
  const videoStuff = videos.find( video => video.id === Number(videoId))
  res.json(videoStuff)
})
app.post('/videos/:id', (req, res) => {
//1) get the new comment out of the request
  let newComment = req.body;
//2) get the id of the current video
  let videoId = req.params.id;
//3) we need to find video with the given id
const videoStuff = videos.find( video => video.id === Number(videoId))
//4) take the new comment and push that to video comment's array
videoStuff.comments.push(newComment)
//5) respond to the request
res.json(videoStuff)
})
app.get('/videos', (req, res) =>{
  res.json(videoLinks);
});
app.listen(8080, () => {
  });

 