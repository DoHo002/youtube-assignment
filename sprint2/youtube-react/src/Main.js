import React, { Component } from 'react';

class Main extends Component {
    render() {
      return (
        <div className='main-container'>
          <video controls className='Video' src = {`${this.props.mainVideo.video}` + "?api_key=1" } />
          <div className='VidTitle'>
            { this.props.mainVideo.title }
          </div>
          <div className='view-container'>
            <div className='viewsNumbers'> { this.props.mainVideo.views } </div>
            <div className='views'> views</div>
            <img className= 'thumbup' src ='../Assets/Icons/Thumbs Up.svg' />
            <div className='Likes'> { this.props.mainVideo.thumbsUp } </div>
            <img className= 'thumbDown' src ='../Assets/Icons/Thumbs Down.svg' />
            <div className='dislikes'> { this.props.mainVideo.thumbsDown } </div>
            <img className='share-button' src ='../Assets/Icons/Share.svg' />
            <div className= 'share'>SHARE</div> 
          </div>
          <div className='channel-tab'>  
            <div className='crop'>
              <div className='grey'></div>
            </div>
            <div className='ch-info'>
              <div className='channel'> { this.props.mainVideo.channel } </div>
              <div className='date'>
                Published on Oct 14 ,2015
              </div>
            </div>
            <div className='subscribe-button'>
              <div className='subscribe'>
                SUBSCRIBE
              </div>
              <div className='sub-count'>
                { this.props.mainVideo.subscriberCount  }
              </div>
            </div>
          </div>
          <div className='show-more-box'>
            <div className='description'>
            { this.props.mainVideo.description }
            </div>
            <div className='more'>
            SHOW MORE
            </div>
          </div>
        </div>  
      );
    }
  }
  
  export default Main;
  