import React, { Component } from 'react'
import { Link } from 'react-router-dom';

export default class VideoCard extends Component {
  render() {
    return (
      <div className='video-card'>
        <div>
          <h4 className='video-card__duration'> {this.props.duration} </h4>
          <Link to={'/main/' + this.props.id }>
            <img className="video-card__image" src={ this.props.image }/>
          </Link>
        </div>
        <div className='vid-description'>
          <h1 className='video-card__title'> { this.props.title } </h1>
          <h2 className='video-card__ch'> { this.props.channel } </h2>
          <h3 className='video-card__views'> { this.props.views } </h3>
          
        </div> 
        
      </div>
    )
  }
}
