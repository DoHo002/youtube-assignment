import React, { Component } from 'react'

export default class Upload extends Component {
  render() {
    return (
      <div className="upload-box">
        <div className='pub-box'>
            <div>Title:</div>
            <input className= 'upload-box__title' type ="text" placeholder = "Add a title to your video"/>
            <div>Description:</div>
            <textarea className= 'upload-box__description' type ="text" placeholder = " Add a description of your video"/>
            <button type='button' className='publish'>Publish</button>
        </div>
      </div>
    )
  }
}
