import React from 'react';
import { Link } from 'react-router-dom';

class NavDisplay extends React.Component{
    render() {
        return (
            <nav className='Nav-bar'>
                <logo >
                    <Link to={'/'}>
                        <img className= 'Nav-bar__f-logo' src ='../Assets/Icons/BrainFlix Logo.svg' />
                    </Link>    
                </logo>
                <input className= 'Nav-bar__search-bar' type ="text" placeholder = "Search"/>  
                <logo className ='Nav-bar__search-logo'>
                    <div className='magnify-back'>
                        <img className='magnify' src = '../Assets/Icons/Search.svg'/>
                    </div>
                    <Link to={'/upload'}>
                        <img className='upload-button' src = '../Assets/Icons/Content Upload.svg' />
                    </Link>    
                    <div className ='gibbonsCircle'>
                        <img className = 'gibbons' src = '../Assets/Images/john_gibbons.jpg' />
                    </div>
                </logo>
            </nav>
        )
    }
}

export default NavDisplay;