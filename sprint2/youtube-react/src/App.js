import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import NavDisplay from './Nav.js';
import Main from './Main.js';
import SideBar from './SideBar.js';
import { BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import Page from './Page.js'
import Upload from './Upload.js';
import Comments from './Comments.js';



class App extends Component {
  
  state = {
    videosArray: [],
    mainVideo: {}
  }

  getMainVideo = (currentUrl) => {
    fetch("https://project-2-api.herokuapp.com/videos/"+ currentUrl +'?api_key=801c6810-b3f0-43d2-9725-b9514948ecdd')
    .then(response => response.json())
    .then(data => {
      this.setState({
        mainVideo: data,
        
      });
    });
  }

  

  componentDidMount(){
    fetch("https://project-2-api.herokuapp.com/videos?api_key=801c6810-b3f0-43d2-9725-b9514948ecdd")
    .then(response => response.json())
    .then(data => {
      this.setState({
        videosArray: data,

      });
    });
    
  }
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <NavDisplay />
            <Switch>
              <Route path={'/main/:vidid'} exact render={ (props) => <Page getMainVideo={ this.getMainVideo } match={props.match} mainVideo={this.state.mainVideo} videosArray={this.state.videosArray}/>}  />
              <Route path={'/upload'} exact component={Upload} />
              <Route path={'/'} render={() => <Redirect to='/main/1edc16bd-1bad-418b-bd40-c72ddd926672'/>} />
            </Switch>
            <Comments />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
