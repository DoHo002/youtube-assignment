const videos = [
    {
        title: 'TEX @ TOR Gm5: Blue Jays take lead in wild 7th inning',
        channel: 'MLB',
        views: '1.1M views',
        duration:'4:45',
        image: './Assets/Images/hqdefault.jpg'
    },
    {
        title: 'TEX @ TOR Gm5: Blue Jays take lead in wild 7th inning',
        channel: 'MLB',
        views: '1.1M views',
        duration:'4:45',
        image: './Assets/Images/donaldson.jpg'
    },
    {
        title: 'TEX @ TOR Gm5: Blue Jays take lead in wild 7th inning',
        channel: 'MLB',
        views: '1.1M views',
        duration:'4:45',
        image: './Assets/Images/r241851_600x400_3-2.jpg'
    },
    {
        title: 'TEX @ TOR Gm5: Blue Jays take lead in wild 7th inning',
        channel: 'MLB',
        views: '1.1M views',
        duration:'4:45',
        image: './Assets/Images/PR6AGOQ7XREI5B7UMKM3KAGWFA.jpg'
    },
    {
        title: 'TEX @ TOR Gm5: Blue Jays take lead in wild 7th inning',
        channel: 'MLB',
        views: '1.1M views',
        duration:'4:45',
        image: './Assets/Images/920x920.jpg'
    },
    {
        title: 'TEX @ TOR Gm5: Blue Jays take lead in wild 7th inning',
        channel: 'MLB',
        views: '1.1M views',
        duration:'4:45',
        image: './Assets/Images/Big-Read-Vladimir-Guerrero-Jr-Swings-470x264.jpg'
    },
    {
        title: 'TEX @ TOR Gm5: Blue Jays take lead in wild 7th inning',
        channel: 'MLB',
        views: '1.1M views',
        duration:'4:45',
        image: './Assets/Images/BASEBALL-MLB-HOU-LAD-.jpg'
    },
    {
        title: 'TEX @ TOR Gm5: Blue Jays take lead in wild 7th inning',
        channel: 'MLB',
        views: '1.1M views',
        duration:'4:45',
        image: './Assets/Images/THKMOYWFLWJCPXQ.20170430201114.jpg'
    }
];

export default videos;