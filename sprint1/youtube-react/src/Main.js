import React, { Component } from 'react';

class Main extends Component {
    render() {
      return (
        <div className='main-container'>
          <video controls className='Video' src = './Assets/Videos/BrainStation Sample Video.mp4' />
          <div className='VidTitle'>
            Jose Bautista hammers go-ahead three-run shot in ALDS Game 5, delivers epic bat flip
          </div>
          <div className='view-container'>
            <div className='viewsNumbers'>2,304,189</div>
            <div className='views'> views</div>
            <img className= 'thumbup' src ='./Assets/Icons/Thumbs Up.svg' />
            <div className='Likes'>6.9K</div>
            <img className= 'thumbDown' src ='./Assets/Icons/Thumbs Down.svg' />
            <div className='dislikes'>202</div>
            <img className='share-button' src ='./Assets/Icons/Share.svg' />
            <div className= 'share'>SHARE</div>
          </div>
          <div className='channel-tab'>  
            <div className='crop'>
              <div className='grey'></div>
            </div>
            <div className='ch-info'>
              <div className='channel'>
                MLB
              </div>
              <div className='date'>
                Published on Oct 14 ,2015
              </div>
            </div>
            <div className='subscribe-button'>
              <div className='subscribe'>
                SUBSCRIBE
              </div>
              <div className='sub-count'>
                1.2M
              </div>
            </div>
          </div>
          <div className='show-more-box'>
            <div className='description'>
            10/14/15: Jose Bautista crushes a long go-ahead three-run homer in the 7th inning of ALDS Game 5
            </div>
            <div className='more'>
            SHOW MORE
            </div>
          </div>
        </div>  
      );
    }
  }
  
  export default Main;
  