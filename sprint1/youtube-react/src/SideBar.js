import React, { Component } from 'react'
import VideoCard from './VideoCard.js';
import videos from './VideoData.js';

export default class SideBar extends Component {
  render() {
      let Data = this.props.videoArray

      let videoBlocks = []
    
      for( let i = 0; i < Data.length; i++){
          let vids = < VideoCard title={ Data[i].title } 
                        channel={ Data[i].channel }
                        views={ Data[i].views }
                        duration={ Data[i].duration }
                        image={ Data[i].image }
                        />
      

      videoBlocks.push(vids);
    }
    return (
      <div>  
        <div className='next'>
            Up next
        </div>  
        <div className="video-loop">
            { videoBlocks }
        </div>
      </div>
    )
  }
}
