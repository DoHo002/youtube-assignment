import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
//you have to import all you js. component to the main App.js file
import NavDisplay from './Nav.js';
import Main from './Main.js';
import videos from './VideoData.js';
import SideBar from './SideBar.js';

class App extends Component {
  state = {
    videoInfo: videos
  }
  render() {
    return (
      <div className="App">
       <NavDisplay />
       <div className='combine'> 
        <Main />
        <SideBar videoArray={this.state.videoInfo} />
       </div> 
      </div>
    );
  }
}

export default App;
