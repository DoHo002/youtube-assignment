//Creating a Nav Bar for the site

//1) BrainFlix logo on the left first

//2) Search Bar with magnify glass

//3) video camera + logo 

//4) profile pic bubble

import React from 'react';

class NavDisplay extends React.Component{
    render() {
        return (
            <nav className='Nav-bar'>
                <logo >
                    <img className= 'Nav-bar__f-logo' src ='./Assets/Icons/BrainFlix Logo.svg' />
                </logo>
                <input className= 'Nav-bar__search-bar' type ="text" placeholder = "Search"/>  
                <logo className ='Nav-bar__search-logo'>
                    <div className='magnify-back'>
                        <img className='magnify' src = './Assets/Icons/Search.svg'/>
                    </div>
                    <img className='upload-button' src = './Assets/Icons/Content Upload.svg' />
                    <div className ='gibbonsCircle'>
                        <img className = 'gibbons' src = './Assets/Images/john_gibbons.jpg' />
                    </div>
                </logo>
            </nav>
        )
    }
}

export default NavDisplay;